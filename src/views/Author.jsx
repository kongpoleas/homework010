
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { uploadImage } from '../services/article_service'
import { addAuthor, fetchAuthor, deleteAuthor, updateAuthor } from '../services/author_service'

function Author() {

    const [author, setAuthor] = useState([])
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    const [id, setId] = useState(null)
    const [isEditing, setIsEditing] = useState(false)

    useEffect(() => {
        fetch();
    }, []);

    const fetch = async () => {
        let author = await fetchAuthor();
        setAuthor(author);
    }

    const onAdd = async (e) => {
        e.preventDefault();
        let author = {
            name, email
        }
        if(imageFile){
            let url = await uploadImage(imageFile)
            author.image = url
        }
        
        document.getElementById("name").value = null;
        document.getElementById("email").value = null;
        document.getElementById("imagePreview").src = "https://designshack.net/wp-content/uploads/placeholder-image.png";
        setImageFile(null);
        fetch()
    }
    const onDelete = async (id) => {
        deleteAuthor(id).then((message) => {
            let newAuthor = author.filter((authorItem) => authorItem._id !== id)
            setAuthor(newAuthor)
        }).catch(e => console.log(e))
    }

    const onUpdate = async (e) => {
        e.preventDefault();
        let author = {
            name, email
        }
        if(imageFile) {
            let url = await uploadImage(imageFile)
            author.image = url
        }
        setId(null);
        document.getElementById("name").value = null;
        document.getElementById("email").value = null;
        document.getElementById("imagePreview").src = "https://designshack.net/wp-content/uploads/placeholder-image.png";
        setImageFile(null);
        setIsEditing(false);
    }

    const onEdit = async (id, name, email, image) => {
        setId(id);
        setName(name)
        setEmail(email)
        setIsEditing(true);
        document.getElementById("name").value = name;
        document.getElementById("email").value = email;
        document.getElementById("imagePreview").src = image;
    }
    return (
        <Container>
            <h1 className="my-3">Author</h1>
            <Row>
                <Col md={9}>
                <Form>
                    <Form.Group>
                        <Form.Label>Author Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Author Name"
                            id="name"
                            onChange={(e)=> setName(e.target.value)} 
                            />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Email"
                            id="email" 
                            onChange={(e) => setEmail(e.target.value)}
                            />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>

                    <Button 
                        variant="primary" 
                        type="submit"
                        onClick={id !== null ? onUpdate : onAdd}
                    >
                    {id ? 'Update' : 'Add'}
                    </Button>
                </Form>
                </Col>
                <Col md={3}>
                    <img id="imagePreview" className="w-100" src={imageURL}/>
                    <Form>
                    <Form.Group>
                        <Form.File 
                            id="img" 
                            label="Choose Image" 
                            onChange={(e)=>{
                                let url = URL.createObjectURL(e.target.files[0])
                                setImageFile(e.target.files[0])
                                setImageURL(url)
                            }}
                            />
                    </Form.Group>
                    </Form>
                </Col>
            </Row>
            <Row className="mt-5">
                <Container>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Image</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            author.map((authorItem, index) => (
                                <tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{authorItem.name}</td>
                                    <td>{authorItem.email}</td>
                                    <td>
                                        <img src={authorItem.image ? authorItem.image: "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"} alt="" width="150px"/>
                                    </td>
                                    <td>
                                    <Button
                                        size="sm"
                                        variant="warning"
                                        onClick = {() => onEdit(authorItem._id, authorItem.name, authorItem.email, authorItem.image)}
                                        >
                                        Edit
                                        </Button>{" "}
                                        <Button
                                        disabled={isEditing}
                                        size="sm"
                                        variant="danger"
                                        onClick={() => onDelete(authorItem._id)}
                                        >
                                        Delete
                                        </Button>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                </Container>
            </Row>
        </Container>
    )
}

export default Author
